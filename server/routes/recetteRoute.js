const express = require('express')
const { getAll, postRecette } = require('../controllers/recetteController')
const route = express.Router()

route.get('/recette/all', getAll)
route.post('/recette/post', postRecette)

module.exports = route