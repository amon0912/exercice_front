const Recette = require("../models/recette")

module.exports = {
    getAll: async (req, res) => {
        const recettes = await Recette.find({})
        return res.status(200).json(recettes)
    },

    postRecette: (req, res) => {
        const newRecette = new Recette(req.body)
        try {
            newRecette.save()
            return res.status(200).json(newRecette)
        } catch (error) {
            return res.status(400).json(error)
        }
    }
}