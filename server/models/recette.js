const mongoose = require("mongoose");

const recetteShema = mongoose.Schema({
    titre: { type: String, require: true },
    timeCook: { type: String, require: true },
    picture: { type: String, require: true }
})

const Recette = mongoose.model('recttes', recetteShema)
module.exports = Recette