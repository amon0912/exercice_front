const express = require('express')
const app = express()
const cors = require('cors')
const bodyparser = require('body-parser')
const mongoose = require('mongoose')
const routeRecette = require('./routes/recetteRoute')

try {
    (async function () {
        await mongoose.connect('mongodb://localhost/cuisine');
    })()
} catch (error) {
    console.log(error);
}

app.use(cors())
app.use(bodyparser.json())
app.use('/api', routeRecette)

app.listen(5000, () => {
    console.log('server en cours');
})
