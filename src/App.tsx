import axios from 'axios';
import React, { useEffect, useState } from 'react';

interface recette {
  titre: string;
  timeCook: string;
  picture: string;

}

const App: React.FC = () => {
  const [recette, setRecette] = useState<recette>({
    titre: '', timeCook: '', picture: ''
  })

  const getRecette = async () => {
    axios.get('http://localhost:5000/api/recette/all')
      .then((result) => {
        const { data } = result
        console.log(data);

      })
      .catch((err) => {
        console.log(err);

      })
  }

  useEffect(() => {
    getRecette()
  }, [])

  return (
    <div >

    </div>
  );
}

export default App;
